FROM ubuntu:19.04

RUN mkdir -p /gc-intro/bin
WORKDIR /gc-intro

RUN apt-get update && apt-get install -y build-essential

COPY /c .
