package org.mpierce.gc

import java.util.concurrent.ThreadLocalRandom

fun main() {
    val data = mutableListOf<ByteArray>()

    while (true) {
        // add some small objects
        repeat(40) {
            data.add(ByteArray(1024))
        }

        // Occasionally, clear out some data
        if (ThreadLocalRandom.current().nextInt(500) == 0) {
            println("Forgetting data, current size = ${data.size}")
            repeat(data.size / 2) {
                data.removeAt(data.size - 1)
            }
        }

        Thread.sleep(1)
    }
}
