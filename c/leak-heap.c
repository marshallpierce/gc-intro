#include <stdio.h>
#include <stdlib.h>

char* make_me_a_string() {
    // make a string in the heap
    char* ptr = calloc(4, sizeof(char));
    // pointers and arrays are interchangeable in C
    ptr[0] = 'f';
    ptr[1] = 'o';
    ptr[2] = 'o';
    // terminating null byte
    ptr[3] = 0;

    return ptr;
}

int main() {
    char* str = make_me_a_string();
    printf("Got a string: %s\n", str);

    // str will stay in memory until the program exits

    return 0;
}
