#!/bin/sh

set -ex

cd `dirname $0`

for f in *.c
do
    cc ${f} -lm -o bin/$(basename ${f} .c)
done
