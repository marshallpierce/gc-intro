#include <stdio.h>
#include <inttypes.h>
#include <math.h>

int* local_int_42() {
    int i = 42;
    int* p = &i;
    return p;
}

void local_float_42() {
    float f = 1.234;
    float rt = sqrtf(f);
    printf("calculated a number: %f\n", rt);
}

int main() {
    int* ptr = local_int_42();

    printf("0x%" PRIXPTR "\n", (uintptr_t) ptr);
    printf("value at ptr: %d\n", *ptr);

    local_float_42();

    printf("value at ptr: %d\n", *ptr);

    return 0;
}
