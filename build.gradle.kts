plugins {
    kotlin("jvm") version "1.3.31"
    application
}

repositories {
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib"))
}

application {
    mainClassName = "org.mpierce.gc.GenerateGarbageKt"
}

tasks.named<JavaExec>("run") {
    args("")
    jvmArgs("-Xlog:gc", "-Xmx4G")
}
