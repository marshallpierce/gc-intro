# C demos

Run `./c/compile.sh` to produce binaries in `./c/bin`. 

If you're on macOS and you want to try running things in Linux, build the provided Dockerfile and run the resulting image.
